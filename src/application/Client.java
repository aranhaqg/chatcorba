package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import chat.*;
import controller.*;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Client extends Application implements Runnable {
	protected static ORB orb;
	protected static String client_id;
	private static ChatServerOperations chatserver = null;
	private static float latitude=0;
	private static float longitude=0;
	private static int ratio=0;
	private static BufferedReader br;

	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws NickAlreadyExistsException, InvalidFieldException {
		ChatClient chatclient = null;
		try {
			//initialize ORB
			orb = ORB.init(args,null);
			//get NameService
			org.omg.CORBA.Object obj = orb.resolve_initial_references("NameService");
			NamingContextExt ncRef = NamingContextExtHelper.narrow(obj);

			//resolve the Object Reference in Naming
			String name = "ChatServer";
			chatserver = ChatServerHelper.narrow(ncRef.resolve_str(name));

			//create servant and connect it to ORB
			ChatClientImpl cc = new ChatClientImpl();
			chatclient = cc._this(orb);
			Thread t = new Thread(new Client());

			br = new BufferedReader(new InputStreamReader(System.in));
			boolean hasNick = false;
			
			askLocation();
			askRatio();
			
			while(!hasNick) {
				try{
					System.out.println("Please type your nick.");
					
					client_id = chatserver.login(br.readLine(), chatclient, longitude, latitude, ratio);
					hasNick = true;
				} catch (NickAlreadyExistsException e) {
					System.err.println("Nick already exists. Choose a new one.");			

				}
			}

			System.out.println("Connected with ID " + client_id+ ".\n");
			System.out.println("-------------------------------------------");
			System.out.println("Type /quit to exit.");
			System.out.println("Type /list to list clients.");
			System.out.println("Type /update_location to update your latitude and longitude.");
			System.out.println("Type /update_ratio to update your search ratio.");
			System.out.println("Type /whoami to get your info.");
			System.out.println("-------------------------------------------");
			t.start();

			while (true) {
				String line = br.readLine();
				switch (line) {
				case "/whoami":
					System.out.println("I am "+chatserver.getNick(client_id)+". Latitude: "+ latitude + "| Longitude:"+ longitude + " | Ratio:" + ratio);
					break;
				case "/quit":
					System.out.println("Exiting chat...");
					chatserver.logout(client_id);
					break;
				case "/list":
					listVisibleClientsNicks();
					break;
				case "/update_ratio":
					askRatio();
					chatserver.updateRatio(ratio,client_id );
					System.out.println("New ratio updated. Listing visible clients in ratio...");
					listVisibleClientsNicks();
					break;
				case "/update_location":
					askLocation();
					
					chatserver.updateLocation(latitude, longitude, client_id);
					
					System.out.println("New location updated. Listing visible clients for new location..");
					listVisibleClientsNicks();
					break;	
					
				default:
					//Message all visible clients
					for(String visibleClientId : chatserver.getVisibleClient_Ids(client_id)){
						ChatClient visibleClient = chatserver.getClient(visibleClientId);
						//Send message directly to instance of ChatClient
						visibleClient.message(chatserver.getNick(client_id), line);
					}
					break;
				}
				
			}
		} catch (ConnectException e){
			System.err.println("Could not connect to server.");
		} catch (InvalidName e) {
			e.printStackTrace();
		} catch (NotFound e) {
			e.printStackTrace();
		} catch (CannotProceed e) {
			e.printStackTrace();
		} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			e.printStackTrace();
		} catch (InvalidFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void askRatio() {
		boolean hasRatio=false;
		while(!hasRatio) {
			try{
				System.out.println("Please type your search ratio in meters.");
				ratio = Integer.parseInt(br.readLine());
				
				hasRatio = true;
			} catch (NumberFormatException e) {
				System.err.println("Please type a valid number for ratio.");			
			} catch (IOException e) {
				System.err.println("Something went wrong. Please try again.");
			}
		}		
	}

	private static void askLocation() {
		boolean hasLocation =false;
		while(!hasLocation) {
			try{
				System.out.println("Please type your latitude.");
				latitude = Float.parseFloat(br.readLine());
				System.out.println("Please type your longitude.");
				longitude = Float.parseFloat(br.readLine());
				
				hasLocation = true;
			} catch (NumberFormatException e) {
				System.err.println("Please type a valid number for latitude and longitude.");			
			} catch (IOException e) {
				System.err.println("Something went wrong. Please try again.");
			}
		}		
	}

	private static void listVisibleClientsNicks() {
		System.out.println("Searching visible clients in ratio...");
		try {
			for(String visibleClientId : chatserver.getVisibleClient_Ids(client_id)){
				System.out.println(">" + chatserver.getNick(visibleClientId));
			}
		} catch (UnknownIDException e) {
			System.err.println("Something went wrong. Please retry.");
		}

	}

	@Override
	public void run() {
		try{
			//get reference to POA and activate POAManager
			org.omg.CORBA.Object obj = orb.resolve_initial_references("RootPOA");
			POA rootpoa = POAHelper.narrow(obj);
			rootpoa.the_POAManager().activate();
			orb.run();
		}catch (Exception e){
		}

	}
}
