package chat;


/**
* chat/_ChatServerStub.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from chat.idl
* Quinta-feira, 3 de Dezembro de 2015 08h01min32s BRT
*/

public class _ChatServerStub extends org.omg.CORBA.portable.ObjectImpl implements chat.ChatServer
{

  public String login (String nick, chat.ChatClient c, float longitude, float latitude, int ratio) throws chat.NickAlreadyExistsException, chat.InvalidFieldException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("login", true);
                $out.write_string (nick);
                chat.ChatClientHelper.write ($out, c);
                $out.write_float (longitude);
                $out.write_float (latitude);
                $out.write_long (ratio);
                $in = _invoke ($out);
                String $result = $in.read_string ();
                return $result;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/NickAlreadyExistsException:1.0"))
                    throw chat.NickAlreadyExistsExceptionHelper.read ($in);
                else if (_id.equals ("IDL:chat/InvalidFieldException:1.0"))
                    throw chat.InvalidFieldExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                return login (nick, c, longitude, latitude, ratio        );
            } finally {
                _releaseReply ($in);
            }
  } // login

  public void logout (String id) throws chat.UnknownIDException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("logout", true);
                $out.write_string (id);
                $in = _invoke ($out);
                return;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/UnknownIDException:1.0"))
                    throw chat.UnknownIDExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                logout (id        );
            } finally {
                _releaseReply ($in);
            }
  } // logout

  public void updateLocation (float latitude, float longitude, String client_id) throws chat.UnknownIDException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("updateLocation", true);
                $out.write_float (latitude);
                $out.write_float (longitude);
                $out.write_string (client_id);
                $in = _invoke ($out);
                return;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/UnknownIDException:1.0"))
                    throw chat.UnknownIDExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                updateLocation (latitude, longitude, client_id        );
            } finally {
                _releaseReply ($in);
            }
  } // updateLocation

  public void updateRatio (int ratio, String client_id) throws chat.UnknownIDException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("updateRatio", true);
                $out.write_long (ratio);
                $out.write_string (client_id);
                $in = _invoke ($out);
                return;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/UnknownIDException:1.0"))
                    throw chat.UnknownIDExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                updateRatio (ratio, client_id        );
            } finally {
                _releaseReply ($in);
            }
  } // updateRatio

  public String getNick (String client_id) throws chat.UnknownIDException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("getNick", true);
                $out.write_string (client_id);
                $in = _invoke ($out);
                String $result = $in.read_string ();
                return $result;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/UnknownIDException:1.0"))
                    throw chat.UnknownIDExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                return getNick (client_id        );
            } finally {
                _releaseReply ($in);
            }
  } // getNick

  public String[] getVisibleClient_Ids (String requester_id) throws chat.UnknownIDException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("getVisibleClient_Ids", true);
                $out.write_string (requester_id);
                $in = _invoke ($out);
                String $result[] = chat.client_idsHelper.read ($in);
                return $result;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/UnknownIDException:1.0"))
                    throw chat.UnknownIDExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                return getVisibleClient_Ids (requester_id        );
            } finally {
                _releaseReply ($in);
            }
  } // getVisibleClient_Ids

  public chat.ChatClient getClient (String client_id) throws chat.UnknownIDException
  {
            org.omg.CORBA.portable.InputStream $in = null;
            try {
                org.omg.CORBA.portable.OutputStream $out = _request ("getClient", true);
                $out.write_string (client_id);
                $in = _invoke ($out);
                chat.ChatClient $result = chat.ChatClientHelper.read ($in);
                return $result;
            } catch (org.omg.CORBA.portable.ApplicationException $ex) {
                $in = $ex.getInputStream ();
                String _id = $ex.getId ();
                if (_id.equals ("IDL:chat/UnknownIDException:1.0"))
                    throw chat.UnknownIDExceptionHelper.read ($in);
                else
                    throw new org.omg.CORBA.MARSHAL (_id);
            } catch (org.omg.CORBA.portable.RemarshalException $rm) {
                return getClient (client_id        );
            } finally {
                _releaseReply ($in);
            }
  } // getClient

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:chat/ChatServer:1.0"};

  public String[] _ids ()
  {
    return (String[])__ids.clone ();
  }

  private void readObject (java.io.ObjectInputStream s) throws java.io.IOException
  {
     String str = s.readUTF ();
     String[] args = null;
     java.util.Properties props = null;
     org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init (args, props);
   try {
     org.omg.CORBA.Object obj = orb.string_to_object (str);
     org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl) obj)._get_delegate ();
     _set_delegate (delegate);
   } finally {
     orb.destroy() ;
   }
  }

  private void writeObject (java.io.ObjectOutputStream s) throws java.io.IOException
  {
     String[] args = null;
     java.util.Properties props = null;
     org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init (args, props);
   try {
     String str = orb.object_to_string (this);
     s.writeUTF (str);
   } finally {
     orb.destroy() ;
   }
  }
} // class _ChatServerStub
