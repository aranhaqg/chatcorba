package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import org.omg.CORBA.ORB;

import chat.*;

public class ChatServerImpl extends ChatServerPOA{
	private ORB orb;
	
	public void setORB(ORB orb_val){
		orb=orb_val;
	}
	
	protected class Client {
		public ChatClient chatclient;
		public String nick;
		public float longitude;
		public float latitude;
		public int ratio;
		public String id;
		public Client(String nick, chat.ChatClient chatclient, float latitude, float longitude, int ratio, String id) {
			this.chatclient = chatclient;
			this.nick = nick;
			this.latitude = latitude;
			this.longitude = longitude;
			this.ratio = ratio;
			this.id = id;
		}
		
		
	}
	protected Map<String, Client> clients = new HashMap<String, Client>();
	protected List<String> nicks = new Vector<String>();
	
	@Override
	public String login(String nick, ChatClient c, float latitude, float longitude, int ratio) throws NickAlreadyExistsException, InvalidFieldException {
		if(nicks.contains(nick)) throw new NickAlreadyExistsException();
		if(latitude==0f | longitude == 0f | ratio == 0) throw new InvalidFieldException();
		nicks.add(nick);
		String client_id = UUID.randomUUID().toString();
		System.out.println("Login: "+nick + "( "+client_id +" )");
		clients.put(client_id, new Client(nick,c,latitude,longitude,ratio, client_id));
		return client_id;
	}

	@Override
	public void logout(String client_id) throws UnknownIDException {
		Client client = clients.remove(client_id);
		if (client==null) throw new UnknownIDException();
		nicks.remove(client.nick);
	}


	@Override
	public void updateRatio(int ratio, String client_id) throws UnknownIDException {
		Client client = clients.get(client_id);
		if (client==null) throw new UnknownIDException();
		
		client.ratio = ratio;
	}

	@Override
	public void updateLocation(float latitude, float longitude, String client_id) throws UnknownIDException {
		Client client = clients.get(client_id);
		if (client==null) throw new UnknownIDException();
		
		client.latitude = latitude;
		client.longitude = longitude;
		
	}


	@Override
	public String[] getVisibleClient_Ids(String requester_id) throws UnknownIDException {
		Client requesterClient = clients.get(requester_id);
		System.out.println("[Visible Clients] Requester: "+ requesterClient.nick);
		ArrayList<String> visibleClients = new ArrayList<String>() ;
		for(Client c : clients.values()){
			if(isInRatio(requesterClient,c)){
				visibleClients.add(c.id);
				System.out.println("Visible client: "+ c.nick);
			}
		}
		
		
		
		return visibleClients.toArray(new String[0]);
	}

	private boolean isInRatio(Client requesterClient, Client c) {
		double distance = DistanceCalculator.distance(requesterClient.latitude, requesterClient.longitude, c.latitude, c.longitude,"K");
		System.out.println("[In Ratio] Requester " + requesterClient.nick + " ratio: "+ requesterClient.ratio
				+ " | Distance in meters to "+c.nick+" : "+ distance*1000 );
		if (requesterClient.ratio>=(distance*1000) ) return true;
		
		return false;
	}

	@Override
	public String getNick(String client_id) throws UnknownIDException {
		Client client = clients.get(client_id);
		if (client==null) throw new UnknownIDException();
		
		return client.nick;
		
	}

	
	@Override
	public ChatClient getClient(String client_id) throws UnknownIDException {
		Client client = clients.get(client_id);
		if (client==null) throw new UnknownIDException();
		
		return client.chatclient;
	}

}
