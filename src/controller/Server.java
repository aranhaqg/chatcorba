package controller;
import chat.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.CosNaming.*;

public class Server {
	public static void main(String args[]) {
		try{
			// initializing ORB
			ORB orb = ORB.init(args,null);

			// get reference to POA and activate POAManager
			POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();
			
			// creating servant and register it with ORB
			ChatServerImpl chatServerImpl = new ChatServerImpl();
			chatServerImpl.setORB(orb);
			
			//get object reference from the servant
			org.omg.CORBA.Object ref = rootpoa.servant_to_reference(chatServerImpl);
			ChatServer href = ChatServerHelper.narrow(ref);
			
			//get the root naming context
			//NameService invokes the name service
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			// Use NamingContextExt which is part of the Interoperable
			// Naming Service (INS) specification.
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
					    
					    
			// bind object reference to NameService
			String name = "ChatServer";
			NameComponent path[] = ncRef.to_name(name);
			ncRef.rebind(path,href);

			System.out.println("ChatServer ready and waiting...");
			
			// wait for invocations from client
			orb.run();

		} catch(Exception e) {
			System.out.println("ERROR: "+ e);
			e.printStackTrace(System.out);
		}
	}
}



