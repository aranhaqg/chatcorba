# README #


### How do I get set up? ###

First, you need to install e(fx)clipse on your Eclipse.

Next, you need to set up External Tool for ORBD at Run > External Tools > External Tool Configurations. Put the location of ORBD, for example "/usr/lib/jvm/java-8-oracle/bin/orbd". Put these arguments: -ORBInitialPort 1050 -ORBInitialHost localhost.

Finnaly, you need to set up Run Configurations for the client and the server at 
Run> Run As > Run Configurations.

For the server, above Java Application add a new configuration poiting to Main Class controller.Server with these arguments: -ORBInitialPort 1050 -ORBInitialHost localhost 

For the client, do the same as you did to server but pointing the Main Class to application.Client with these arguments: -ORBInitialPort 1050 -ORBInitialHost localhost
 
## Dependencies

* e(fx)clipse
* Java 8

### How to I run? ###


1. Run only one instance of orbd;
2. Run only one instance of server;
3. Run as many as you want instances of clients ;

### Test Case ###

* Ratio example for all: 512 000 m;
* Nick: client1 |  Latitude: -3.731862 | Longitude: -38.526670 (Fortaleza)
* Nick: client2 | Latitude: -5.779257 | Longitude: -35.200916 (Natal)
* Nick: client3 | Longitude 52.520007 | Longitude 13.404954 (Berlin)
* Expected behavior: client1 and client2 should be visible to each other and see and send their messages; client3 will only be seen and see the others clients with ratio for all changed to a greater distance than Berlin to Natal (6.996,78 km) and to Berlin to Fortaleza (7.916,09 km).

### Who do I talk to? ###

* Repo owner or admin (aranhaqg@gmail.com)